AUTHOR = 'henrique souza'
SITENAME = 'zone option'
SITEURL = 'zoneoption.net'
SITEURL = '/zoneoption/'

PATH = 'content'

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = "./rss.xml"
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = './themes/meaningless'

ARTICLE_PATHS = ['articles', 'wiki','micro']

ARTICLE_URL = '{category}/{date:%Y}-{date:%m}-{date:%d}/{slug}.html'

ARTICLE_SAVE_AS = '{category}/{date:%Y}-{date:%m}-{date:%d}/{slug}.html'

MARKDOWN = {
  'extension_configs': {
    'markdown.extensions.toc': {
      'permalink': 'true',
      'permalink_title': 'permanent link'
    },
    'markdown.extensions.codehilite': {
    },
    'markdown.extensions.footnotes': {
    },
  }
}

# PLUGINS = ['simple_footnotes']

STATIC_PATHS = [ 'static', 'images']

EXTRA_PATH_METADATA = {
    'static/robots.txt': {'path': 'robots.txt'},
}
